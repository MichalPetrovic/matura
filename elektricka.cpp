#include <iostream>
#include <ctime>
#include <cstdlib>
#include <iomanip>

using namespace std;

void elektricka(int zac, int kon, int interval)
{
    int y = 0;
    for (int i = zac; i <= kon; i++) {
        while (y < 60) {
            if (y == 0) {
                cout << setw(5) << i << ":"
                     << "00";
            }
            else {
                if (y < 10) {
                    cout << setw(5) << i << ":"
                         << "0" << y;
                }
                else
                    (cout << setw(5) << i << ":" << y);
            }
            y = y + interval;
        }
        y = y - 60;
        cout << endl;
    }
}

int main()
{
    int zac, kon, interval;
    cout << "zadaj zaciatocnu hodinu" << endl;
    cin >> zac;
    cout << "zadaj koncovu hodinu" << endl;
    cin >> kon;
    cout << "zadaj interval" << endl;
    cin >> interval;
    elektricka(zac, kon, interval);
    return 0;
}
