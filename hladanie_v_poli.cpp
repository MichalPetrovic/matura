#include <iostream>
#include <stdlib.h>

using namespace std;

int hladanie(int pole[], int prvy, int posledny, int cislo)
{
    int index;

    if (prvy > posledny)
        index = -1;

    else {
        int stredny = (prvy + posledny) / 2;

        if (cislo == pole[stredny])
            index = stredny;
        else

            if (cislo < pole[stredny])
            index = hladanie(pole, prvy, stredny - 1, cislo);
        else
            index = hladanie(pole, stredny + 1, posledny, cislo);

    }
    return index;
}

void bubbleSort(int* array, int size)
{
    for (int i = 0; i < size - 1; i++) {
        for (int j = 0; j < size - i - 1; j++) {
            if (array[j + 1] < array[j]) {
                int tmp = array[j + 1];
                array[j + 1] = array[j];
                array[j] = tmp;
            }
        }
    }
}

int main()
{
    int pole[20];
    int num;
    for (int i = 0; i < 20; i++) {
        pole[i] = rand() % 99;
    }

    bubbleSort(pole, 20);
    for (int i = 0; i < 20; i++)
        cout << pole[i] << " ";
    cout << endl;
    cin >> num;
    int position = hladanie(pole, 1, 21, num);

    cout << "Nachadza sa na: " << position + 1;

    return 0;
}
