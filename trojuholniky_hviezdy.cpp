#include <iostream>

using namespace std;

void drawStars(int poc_troj, int poc_hviezd)
{
    for (int i = 1; i <= poc_hviezd; i++) {
        for (int j = 1; j <= poc_troj; j++) {
            for (int k = 1; k <= i; k++) {
                cout << "* ";
            }
            for (int l = 1; l <= poc_hviezd - i; l++) {
                cout << "  ";
            }
        }

        cout << endl;
    }
}

int main()
{
    int poc_troj, poc_hviezd;
    cout << "Zadaj pocet trojuholnikov: ";
    cin >> poc_troj;
    cout << "\nZadaj pocet hviezd: ";
    cin >> poc_hviezd;

    drawStars(poc_troj, poc_hviezd);
    return 0;
}
