#include <iostream>
#include <cstdlib>
#include <ctime>
#include <iomanip>

using namespace std;

int main()
{
    srand(time(0));
    int pole[10], maxx = 0;
    for (int i = 0; i < 10; i++) {
        pole[i] = rand() % (10);
        cout << setw(3) << pole[i];
        if (pole[i] > maxx) {
            maxx = pole[i];
        }
    }
    cout << endl;

    for (int j = maxx; j >= 1; j--) {
        for (int i = 0; i < 10; i++) {
            if (pole[i] >= j) {
                cout << setw(3) << "*";
            }
            else {
                cout << setw(3) << " ";
            }
        }
        cout << endl;
    }

    return 0;
}
