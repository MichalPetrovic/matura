int hladanie(int pole[], int prvy, int posledny, int cislo)
{
    int index;

    if (prvy > posledny)
        index = -1;

    else {
        int stredny = (prvy + posledny) / 2;

        if (cislo == pole[stredny])
            index = stredny;
        else

            if (cislo < pole[stredny])
            index = hladanie(pole, prvy, stredny - 1, cislo);
        else
            index = hladanie(pole, stredny + 1, posledny, cislo);

    }
    return index;
}