#include <iostream>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <string>

using namespace std;

int main()
{
    int usr_choice;
	int comp_choice;
    int usr_skore = 0;
    int comp_skore = 0;

    srand(time(0));
    
    for (int i = 0; i < 5; i) {
        cout << "1 - kamen; 2 - papier; 3 - noznice: ";
        cin >> usr_choice;
        comp_choice = rand() % 3 + 1;
        cout << "Pocitac dal: " << comp_choice;
        if (usr_choice > 3 || usr_choice < 1) {
            cout << " zla volba ";
            i--;
        }
        if (usr_choice == comp_choice) {
            cout << " remiza!";
        }
        if (usr_choice == 1 && comp_choice == 3) {
            cout << " vyhral si!";
            usr_skore++;
            i++;
        }
        if (usr_choice == 1 && comp_choice == 2) {
            cout << " prehral si!";
            comp_skore++;
            i++;
        }
        if (usr_choice == 2 && comp_choice == 1) {
            cout << " vyhral si!";
            usr_skore++;
        }
        if (usr_choice == 2 && comp_choice == 3) {
            cout << " prehral si!";
            comp_skore++;
        }
        if (usr_choice == 3 && comp_choice == 1) {
            cout << " prehral si!";
            comp_skore++;
            i++;
        }
        if (usr_choice == 3 && comp_choice == 2) {
            cout << " vyhral si!";
            usr_skore++;
            i++;
        }
        cout << endl;
    }

    if (usr_skore > comp_skore) {
        cout << "\nCelkovo vyhral clovek " << usr_skore << ":" << comp_skore;
    }
    else if (usr_skore < comp_skore) {
        cout << "\nCelkovo vyhral pocitac " << usr_skore << ":" << comp_skore;
    }
    else {
        cout << "\nRemiza! " << usr_skore << ":" << comp_skore;
    }
    return 0;
}
