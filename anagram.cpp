#include <iostream>
#include <algorithm>

using namespace std;

bool anagram(string s1, string s2){
    sort(s1.begin(), s1.end());
    sort(s2.begin(), s2.end());

    return s1==s2;
}

int main()
{
    string slovo1, slovo2;
    cin>>slovo1;
    cin>>slovo2;
    anagram(slovo1, slovo2) ? cout<<"Je to anagram" : cout<<"Nie je to anagram";

    return 0;
}
